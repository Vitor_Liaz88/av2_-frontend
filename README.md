# Projeto Front-End Singleton

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.13.

```bash
#Instale o angular na versão 9.1.13 (não obrigatório)
$npm install -g @angular/cli@9.1.13

#Instale o angular (obrigatório)
$npm install -g @angular/cli

# Clone o repositório
$ git clone <https://gitlab.com/Vitor_Liaz88/av2_-frontend.git>

# Instale as depêndencias
$ npm install

# Execute a aplicação
$ yarn dev

```
## Executar a aplicação em modo de desenvolvimento

No terminal do diretório do projeto execute o comando `ng server`. Acesse a página no endereço `http://localhost:4200/`.


