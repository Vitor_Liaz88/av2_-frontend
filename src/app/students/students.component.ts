import { Component, OnInit } from '@angular/core';
import { StudentsService } from './students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss'],
})
export class StudentsComponent implements OnInit {
  public studentsList;
  public headers = ['name', 'registro', 'av1', 'av2', 'av3'];
  constructor(private studentService: StudentsService) {}

  ngOnInit(): void {
    this.studentsList = this.studentService.getStudents();
  }
}
