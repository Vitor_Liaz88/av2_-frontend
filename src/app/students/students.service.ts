import { Injectable } from '@angular/core';

export interface Student {
  name: string,
  registro: number,
  av1?: string,
  av2?:string,
  av3?: string
};

@Injectable({
  providedIn: 'root'
})

export class StudentsService {
  private students: Array<Student> = [
    { name: "Vitor", registro: 18152880043 },
    { name: "Maicon", registro: 123123 },
    { name: "Joao", registro: 345345 },
    { name: "Nivaldo", registro: 567567 },
    { name: "Gabriella", registro:986546}
  ]
  constructor() {
    console.log('Student Service');
  }

  public getStudents() {
    return this.students;
  }

  getByRegister(resgistration) {
    return this.students.find((ele) => ele.registro == resgistration);
  }

  saveByRegister(registration, student) {
    const index = this.students.findIndex((ele) => ele.registro == registration);
    this.students[index] = student;
    console.log("Atualizado",this.students);
  }
}
