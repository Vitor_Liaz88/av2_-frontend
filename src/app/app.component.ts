import { Component } from '@angular/core';
import { StudentsService } from './students/students.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private studentService: StudentsService){};
  title = 'angular-singleton';
}
