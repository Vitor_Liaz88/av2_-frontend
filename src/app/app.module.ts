import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StudentsService } from './students/students.service';
import { StudentsModule } from './students/students.module';
import { TeacherModule } from './teacher/teacher.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StudentsModule,
    TeacherModule,
    MatToolbarModule,
    MatIconModule,
    SharedModule
  ],
  providers: [StudentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

