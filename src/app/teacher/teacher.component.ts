import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../students/students.service';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss'],
})
export class TeacherComponent implements OnInit {
  public studentsList;
  public headers = ['name', 'registro', 'av1', 'av2', 'av3', 'actions'];
  constructor(private studentService: StudentsService) {}

  ngOnInit(): void {
    this.studentsList = this.studentService.getStudents();
  }
}
