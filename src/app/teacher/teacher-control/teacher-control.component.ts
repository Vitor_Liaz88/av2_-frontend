import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { StudentsService } from 'src/app/students/students.service';

@Component({
  selector: 'app-teacher-control',
  templateUrl: './teacher-control.component.html',
  styleUrls: ['./teacher-control.component.scss'],
})
export class TeacherControlComponent implements OnInit, OnDestroy {
  private routeSub: Subscription;
  public registration;
  public profile;
  public studentForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private studentService: StudentsService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      this.registration = params['id'];
      console.log(params);
      console.log(params['id']);
    });
    console.log(this.registration);
    this.loadStudentProfile(this.registration);
    this.buildForm();
    this.studentForm.patchValue(this.profile);
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  loadStudentProfile(registration:number) {
    this.profile = this.studentService.getByRegister(registration);
    console.log(this.profile);
  }

  buildForm() {
    this.studentForm = this.formBuilder.group({
      name: [ null, Validators.required],
      registro: [null, Validators.required],
      av1: [null],
      av2: [null],
      av3: [null],
    });
  }

  save() {
    this.studentService.saveByRegister(this.registration, this.studentForm.value);
  }

}
